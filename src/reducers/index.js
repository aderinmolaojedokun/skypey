import user from "./user";
import contact from "./contacts";
import activeUserId from "./activeUserId";
import { combineReducers } from "redux";

export default combineReducers({
  user,
  contact,
  activeUserId,
});
