import React from "react";
import "./App.css";
import Main from "../components/Main/Main";
import Sidebar from "../components/Sidebar/Sidebar";
import _ from "lodash";
import store from "../store";

const App = () => {
  const { contact, user, activeUserId } = store.getState();
  return (
    <div className="App">
      <Sidebar contacts={_.values(contact)} />
      <Main user={user} activeUserId={activeUserId} />
    </div>
  );
};

export default App;
